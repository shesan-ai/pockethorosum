from flask import Flask, render_template, request
from main import HoroscopeSummarizer

app = Flask(__name__)

@app.route('/', methods=['GET','POST'])
def index():
    input_text = request.form.get('inputText',None)
    if input_text is not None:
        horosum = HoroscopeSummarizer()
        summary = horosum.summarize(input_text)
    else:
        input_text = ''
        summary = ''
    
    data = {
        'input_text':input_text,
        'summary':summary
    }
    return render_template('index.html',**data)

# @app.route('/summarize', methods=['POST'])
# def summarize_text():
#     input_text = request.form['inputText']
#     summarized_text = "Your summarized text goes here..."  # Implement your summarization logic here
#     return render_template('summarizer.html', summarized_text=summarized_text)

if __name__ == '__main__':
    app.run(debug=True)
