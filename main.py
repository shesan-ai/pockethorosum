import os
from transformers import T5Tokenizer,T5ForConditionalGeneration
import torch
# # from transformers import pipeline

# text = """
# Hello Aquarius! You may have to experience some stress today. You may find yourself stressing about your dietary choices. You may feel stress due to your worries or concerns. So, take some time for self-care and find ways to relax. You might face some challenges in pursuing higher education. It's possible that you may feel stressed related to your financial investments or assets. You may experience some stress and conflicts within family dynamics or household. Public speaking, presenting ideas, or expressing yourself verbally may create anxiety or worry. Lastly, recognize your strength to overcome these doubts and trust in your own capabilities.
# """

# formatted_text = "summary of the content:: " + text

# model_path = "./horoscope_t5_small/"

# # device = 'cuda' if torch.cuda.is_available() else 'cpu'
# device = 'cpu'

# tokenizer = T5Tokenizer.from_pretrained(model_path,use_fast=False)
# inputs = tokenizer(text, return_tensors="pt",max_length=512,truncation=True,padding='max_length').to(device).input_ids

# model = T5ForConditionalGeneration.from_pretrained(model_path)
# model.to(device)
# # outputs = model.generate(inputs, max_new_tokens=128, do_sample=True, top_k=50, top_p=0.9)
# outputs = model.generate(inputs, max_new_tokens=128, num_beams=2)

# summary = tokenizer.decode(outputs[0], skip_special_tokens=True)

# print(summary)
# # summarizer = pipeline("summarization", model="./horoscope_t5_small/")
# # pred = summarizer(formatted_text, max_new_tokens=128)
# # print(pred)


class HoroscopeSummarizer():
    def __init__(self):
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        # self.device = 'cpu'
        model_name = './horosum_model/' if os.path.exists(os.path.join(os.getcwd(), 'horosum_model')) else 'shesan/horosum'

        self.model = T5ForConditionalGeneration.from_pretrained(model_name)
        self.tokenizer = T5Tokenizer.from_pretrained(model_name,use_fast=False)
        if model_name == 'shesan/horosum':
            self.model.save_pretrained('./horosum_model/')
            self.tokenizer.save_pretrained('./horosum_model/')
        self.model.to(self.device)
    
    def summarize(self,text):
        instruction = "summarize:: "
        formatted_text = instruction + text
        tokenized = self.tokenizer(formatted_text, return_tensors="pt", max_length=512, truncation=True, padding='max_length', add_special_tokens=True)
        tokenized.to(self.device)
        inputs = tokenized.input_ids
        outputs = self.model.generate(inputs, max_new_tokens=150, num_beams=2, repetition_penalty=2.0, use_cache=False)
        summary = self.tokenizer.decode(outputs[0], skip_special_tokens=True)
        return summary