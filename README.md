## Readme

### Initial setup
> Prerequsites : Make sure that git is installed and properly setup. Also verify
> that pipenv is installed globally in your system

1. Clone from remote git repository
    * `$ git clone <remote_addr>`
2. CD into project root
    * `$ cd <project-root>`
3. Activate virtual environemnt
    * `$ pipenv shell`
4. Install packages
    * `$ make install`
5. Run the development server
    * `$ make server`
6. Navigate to [localhost:5000](http://localhost:5000) in web browser